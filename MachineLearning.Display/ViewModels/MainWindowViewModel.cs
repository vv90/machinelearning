﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Prism.ViewModel;
using OxyPlot;

namespace MachineLearning.Display.ViewModels
{
	public class MainWindowViewModel : BindableBase
	{
		public const int SampleLength = 10;
		private IContinuousDistribution _distribution = new ContinuousUniform(-1.0, 1.0);

		private DelegateCommand _generateCommand; 

		public List<DataPoint> SourcePoints { get; set; }
		public List<DataPoint> SamplePointsTrue { get; set; }
		public List<DataPoint> SamplePointFalse { get; set; }
		

		public MainWindowViewModel()
		{
			GenerateCommandExecute();
		}

		public ICommand GenerateCommand 
		{ 
			get
			{
				return _generateCommand ?? (_generateCommand = new DelegateCommand(GenerateCommandExecute));
			}
		}

		private void GenerateCommandExecute()
		{
			GenerateNonlinearTransformPoints();
		}

		private void GenerateNonlinearTransformPoints()
		{
			var targetFunction = new Func<Vector<double>, double>(
				vector => Math.Pow(vector[0], 2) + Math.Pow(vector[1], 2) - 0.6);

			var distribution = new ContinuousUniform(-1.0, 1.0);
			var sample = Enumerable.Range(0, 1000)
				.Select(item =>
				{
					var point = Vector.Build.Random(2, distribution);
					bool flip = distribution.Sample() > 0.8;
					return new SamplePoint { Point = point, Value = (!flip && targetFunction(point) > 0) };
				})
				.ToArray();

			SamplePointsTrue = new List<DataPoint>(sample
				.Where(p => p.Value)
				.Select(p => new DataPoint(p.Point[0], p.Point[1])));

			SamplePointFalse = new List<DataPoint>(sample
				.Where(p => !p.Value)
				.Select(p => new DataPoint(p.Point[0], p.Point[1])));

			OnPropertyChanged(() => SourcePoints);
			OnPropertyChanged(() => SamplePointsTrue);
			OnPropertyChanged(() => SamplePointFalse);
		}

		private void GeneratePerceptronPoints()
		{
			var point1 = Vector.Build.Random(2, _distribution);
			var point2 = Vector.Build.Random(2, _distribution);

			var targetFunction = new LinearFurction(point1, point2);
			var sample = new SamplePoint[SampleLength];

			for (int i = 0; i < sample.Length; i++)
			{
				var point = Vector.Build.Random(2, _distribution);
				sample[i] = new SamplePoint
				{
					Point = point,
					Value = targetFunction.DistanceFromLine(point[0], point[1]) > 0
				};
			}

			SourcePoints = new List<DataPoint>
			{
				new DataPoint(point1[0], point1[1]), 
				new DataPoint(point2[0], point2[1])
			};

			SamplePointsTrue = new List<DataPoint>(sample
				.Where(p => p.Value)
				.Select(p => new DataPoint(p.Point[0], p.Point[1])));

			SamplePointFalse = new List<DataPoint>(sample
				.Where(p => !p.Value)
				.Select(p => new DataPoint(p.Point[0], p.Point[1])));

			var perceptron = new Perceptron();

			OnPropertyChanged(() => SourcePoints);
			OnPropertyChanged(() => SamplePointsTrue);
			OnPropertyChanged(() => SamplePointFalse);
		}
	}
}
