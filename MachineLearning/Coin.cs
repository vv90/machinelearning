﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.Distributions;

namespace MachineLearning
{
	public class Coin
	{
		private static DiscreteUniform _disrtibution = new DiscreteUniform(1, 2);

		public CoinValue Flip()
		{
			return (CoinValue) _disrtibution.Sample();
		}
	}
}
