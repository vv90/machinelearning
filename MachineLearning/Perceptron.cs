﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace MachineLearning
{
	public class Perceptron
	{
		public Vector<double> Weights { get; private set; }
		
		public int Learn(IEnumerable<SamplePoint> sample, Vector<double> initialWeights = null)
		{
			if(!sample.Any())
			{
				throw new ArgumentException("The sample is empty. Cannot learn from an empty sample");
			}

			Weights = initialWeights ?? Vector.Build.Dense(sample.First().Point.Count + 1, i => 0);
			var iterationCount = 0;
			var learningSet = sample
				.Select(s => new LearningSetItem
				{
					Point = s.Point,
					SampleValue = s.Value ? 1 : -1,
					ComputedValue = Evaluate(s.Point)
				}).ToArray();

			

			while (learningSet.Any(s => s.ComputedValue != s.SampleValue))
			{
				var misclassifiedItem = learningSet.First(s => s.ComputedValue != s.SampleValue);
				Weights = Weights + AppendX0(misclassifiedItem.Point)*misclassifiedItem.SampleValue;

				foreach (var p in learningSet)
				{
					p.ComputedValue = Evaluate(p.Point);
				}

				iterationCount += 1;
			}
			
			return iterationCount;
		}

		public bool Classify(Vector<double> point)
		{
			return Evaluate(point) > 0;
		}

		public double OutOfSampleError(LinearFurction targetFunction, IContinuousDistribution distribution, int trials = 10000)
		{
			var errorCount = 0;
			for (int i = 0; i < trials; i++)
			{
				var point = Vector.Build.Random(2, distribution);
				var perceptronValue = Classify(point);
				var targetFunctionValue = targetFunction.DistanceFromLine(point[0], point[1]) > 0;

				if (perceptronValue != targetFunctionValue)
					errorCount += 1;
			}

			return (double) errorCount/trials;
		}

		private int Evaluate(Vector<double> point)
		{
			//var input = Vector.Build.Dense(_weights.Count, i => i == 0 ? 1 : point[i - 1]);

			var result = Weights.DotProduct(AppendX0(point)) > 0 ? 1 : -1;

			return result;
		}

		private Vector<double> AppendX0(Vector<double> point)
		{
			return Vector.Build.Dense(point.Count + 1, i => i == 0 ? 1 : point[i - 1]);
		}
	}

	public class LearningSetItem
	{
		public Vector<double> Point { get; set; }
		public int SampleValue { get; set; }
		public int ComputedValue { get; set; }
	}

	public class SamplePoint
	{
		public Vector<double> Point { get; set; }
		public bool Value { get; set; }
	}
}
