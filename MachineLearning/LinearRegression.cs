﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace MachineLearning
{
	public class LinearRegression
	{
		public Vector<Double> Weights { get; private set; }
		private SamplePoint[] _sample;
		private Func<Vector<double>, Vector<double>> _transformFunction;

		public LinearRegression()
		{
			_transformFunction = (vector => vector);
		}

		public LinearRegression(Func<Vector<double>, Vector<double>> transformFunction)
		{
			_transformFunction = transformFunction;
		}
		public void Learn(IEnumerable<SamplePoint> sample)
		{
			_sample = sample.ToArray();
			if (!_sample.Any())
			{
				throw new ArgumentException("The sample is empty. Cannot learn from an empty sample");
			}

			var matrix = DenseMatrix.Build.DenseOfRowVectors(_sample.Select(s => AppendX0(_transformFunction(s.Point))));
			var vector = Vector.Build.DenseOfArray(_sample.Select(s => s.Value ? 1.0 : -1.0).ToArray());
			//var matrixTransposed = matrix.Transpose();
			var pseudoInverse = matrix.QR().Solve(DenseMatrix.CreateIdentity(matrix.RowCount));

			//Weights = ((matrixTransposed*matrix).Inverse()*matrixTransposed)*vector;
			Weights = pseudoInverse*vector;
		}

		public bool Classify(Vector<double> point)
		{
			return Evaluate(point) > 0;
		}

		public double Evaluate(Vector<double> point)
		{
			return Weights.DotProduct(AppendX0(_transformFunction(point)));
		}

		public double OutOfSampleError(Func<Vector<double>, double> targetFunction, ContinuousUniform distribution, int trials = 10000)
		{
			var errorCount = 0;
			for (int i = 0; i < trials; i++)
			{
				var point = Vector.Build.Random(2, distribution);
				var value = Classify(point);
				var targetFunctionValue = targetFunction(point) > 0;

				if (value != targetFunctionValue)
					errorCount += 1;
			}

			return (double)errorCount / trials;	
		}

		public double InSampleError()
		{
			var errorCount = 0;
			foreach (var p in _sample)
			{
				var value = Classify(p.Point);
				if (value != p.Value)
					errorCount += 1;
			}

			return (double) errorCount/_sample.Length;
		}

		private Vector<double> AppendX0(Vector<double> point)
		{
			return Vector.Build.Dense(point.Count + 1, i => i == 0 ? 1 : point[i - 1]);
		}
	}
}
