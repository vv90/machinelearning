﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace MachineLearning
{
	public class LinearFurction
	{
		public const double Epsilon = 1E-14;

		private Vector<double> _point1;
		private Vector<double> _point2;

		public LinearFurction(Vector<double> point1, Vector<double> point2)
		{
			if (Math.Abs(point1[0] - point2[0]) < Epsilon && Math.Abs(point1[1] - point2[1]) < Epsilon)
				throw new ArgumentException("Points must be different");

			_point1 = point1;
			_point2 = point2;
		}


		public double DistanceFromLine(double x, double y)
		{
			// line equation a*x + b*y + c = 0

			double x1 = _point1[0], y1 = _point1[1], x2 = _point2[0], y2 = _point2[1];
			double a = y1 - y2, b = x2 - x1, c = x1 * y2 - x2 * y1;

			var sign = Math.Sign(c) < 0 ? -1 : 1;
			return (a * x + b * y + c) / (sign * Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2)));
		}

		public IEnumerable<SamplePoint> GenerateSample(IContinuousDistribution distribution, int sampleLength)
		{
			var sample = new List<SamplePoint>();

			for (int i = 0; i < sampleLength; i++)
			{
				var point = Vector.Build.Random(2, distribution);
				sample.Add(new SamplePoint { Point = point, Value = DistanceFromLine(point[0], point[1]) > 0 });
			}

			return sample;
		}
	}
}
