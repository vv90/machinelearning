﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MachineLearning;
using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra.Double;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MachineLearningTest
{
	[TestClass]
	public class PerceptronTest
	{
		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void TestLearnThrowsWithEmptySample()
		{
			var target = new Perceptron();
			var sample = new SamplePoint[0];

			target.Learn(sample);
		}

		[TestMethod]
		public void TestLearn()
		{
			var target = new Perceptron();
			var sample = new SamplePoint[]
			{
				new SamplePoint { Point = Vector.Build.Dense(new []{ -1.0, 0.1 }), Value = true },
				new SamplePoint { Point = Vector.Build.Dense(new []{ -0.7, 0.1 }), Value = true },
				new SamplePoint { Point = Vector.Build.Dense(new []{ -0.5, 0.1 }), Value = true },
				new SamplePoint { Point = Vector.Build.Dense(new []{ 0.1, 0.1 }), Value = true },
				new SamplePoint { Point = Vector.Build.Dense(new []{ 0.4, 0.1 }), Value = true },

				new SamplePoint { Point = Vector.Build.Dense(new []{ -1.0, -0.1 }), Value = false },
				new SamplePoint { Point = Vector.Build.Dense(new []{ -0.7, -0.1 }), Value = false },
				new SamplePoint { Point = Vector.Build.Dense(new []{ -0.5, -0.1 }), Value = false },
				new SamplePoint { Point = Vector.Build.Dense(new []{ 0.1, -0.1 }), Value = false },
				new SamplePoint { Point = Vector.Build.Dense(new []{ 0.4, -0.1 }), Value = false }
			};

			target.Learn(sample);

			foreach (var p in sample)
			{
				Assert.AreEqual(p.Value, target.Classify(p.Point));
			}
		}

		[TestMethod]
		public void TestConvergence()
		{
			object syncObject = new object();
			var errorSum = 0.0;
			var convergenceCountSum = 0;
			var trials = 1000;

			var stopwatch = new Stopwatch();

			stopwatch.Start();

			Parallel.For(0, trials, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, i =>
			{
				var distribution = new ContinuousUniform(-1.0, 1.0);
				var point1 = Vector.Build.Random(2, distribution);
				var point2 = Vector.Build.Random(2, distribution);
				var targetFunction = new LinearFurction(point1, point2);
				var sample = targetFunction.GenerateSample(distribution, 10);
				var perteptron = new Perceptron();
				var convegedAfter = perteptron.Learn(sample);
				var outOfSampleError = perteptron.OutOfSampleError(targetFunction, distribution);

				lock (syncObject)
				{
					errorSum += outOfSampleError;
					convergenceCountSum += convegedAfter;
				}
			});

			stopwatch.Stop();

			var averageError = errorSum / trials;
			var averageConvergenceCount = (double)convergenceCountSum / trials;
			var elapsedTime = stopwatch.ElapsedMilliseconds;


			Assert.IsTrue(true);
		}

		[TestMethod]
		public void TestConvergenceWithInitialWeights()
		{
			object syncObject = new object();
			var errorSum = 0.0;
			var convergenceCountSum = 0;
			var trials = 1000;

			var stopwatch = new Stopwatch();

			stopwatch.Start();

			Parallel.For(0, trials, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, i =>
			{
				var distribution = new ContinuousUniform(-1.0, 1.0);
				var point1 = Vector.Build.Random(2, distribution);
				var point2 = Vector.Build.Random(2, distribution);
				var targetFunction = new LinearFurction(point1, point2);
				var sample = targetFunction.GenerateSample(distribution, 10).ToArray();

				var linearRegression = new LinearRegression();
				linearRegression.Learn(sample);

				var perteptron = new Perceptron();
				var convegedAfter = perteptron.Learn(sample, linearRegression.Weights);
				var outOfSampleError = perteptron.OutOfSampleError(targetFunction, distribution);

				lock (syncObject)
				{
					errorSum += outOfSampleError;
					convergenceCountSum += convegedAfter;
				}
			});

			stopwatch.Stop();

			var averageError = errorSum / trials;
			var averageConvergenceCount = (double)convergenceCountSum / trials;
			var elapsedTime = stopwatch.ElapsedMilliseconds;


			Assert.IsTrue(true);
		}
	}
}
