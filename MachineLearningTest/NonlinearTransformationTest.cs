﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using MachineLearning;
using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MachineLearningTest
{
	[TestClass]
	public class NonlinearTransformationTest
	{
		[TestMethod]
		public void NonlinearDataWithoutTransformation()
		{
			var trials = 1000;
			var inSampleErrorSum = 0.0;
			for (int i = 0; i < trials; i++)
			{
				var targetFunction = new Func<Vector<double>, double>(
					vector => Math.Pow(vector[0], 2) + Math.Pow(vector[1], 2) - 0.6);

				var distribution = new ContinuousUniform(-1.0, 1.0);
				var sample = Enumerable.Range(0, 1000)
					.Select(item =>
					{
						var point = Vector.Build.Random(2, distribution);
						bool flip = distribution.Sample() > 0.8;
						return new SamplePoint {Point = point, Value = (!flip && targetFunction(point) > 0)};
					});

				var linearRegression = new LinearRegression();
				linearRegression.Learn(sample);
				inSampleErrorSum += linearRegression.InSampleError();
			}

			var averageInSampleError = inSampleErrorSum/trials;
			
			Assert.IsTrue(true);
		}

		[TestMethod]
		public void NonlinearDataWithTransformation()
		{
			var trials = 1000;
			var inSampleErrorSum = 0.0;
			var outOfSampleErrorSum = 0.0;

			for (int i = 0; i < trials; i++)
			{
				var targetFunction = new Func<Vector<double>, double>(
						vector => Math.Pow(vector[0], 2) + Math.Pow(vector[1], 2) - 0.6);
				var transformFunction = new Func<Vector<double>, Vector<double>>(
					vector => Vector.Build.Dense(new[]
				{
					vector[0], vector[1], vector[0], vector[1], Math.Pow(vector[0], 2), Math.Pow(vector[1], 2)
				}));

				var distribution = new ContinuousUniform(-1.0, 1.0);
				var sample = Enumerable.Range(0, 1000)
					.Select(item =>
					{
						var point = Vector.Build.Random(2, distribution);
						bool flip = distribution.Sample() > 0.8;
						return new SamplePoint { Point = point, Value = (!flip && targetFunction(point) > 0) };
					});

				var linearRegression = new LinearRegression(transformFunction);
				linearRegression.Learn(sample);
				inSampleErrorSum += linearRegression.InSampleError();
				outOfSampleErrorSum += linearRegression.OutOfSampleError(targetFunction, distribution);
			}

			var averageInSampleError = inSampleErrorSum/trials;
			var averagOutOfSampleError = outOfSampleErrorSum/trials;

			Assert.IsTrue(true);
		}
	}
}
