﻿using System;
using System.Reflection;
using MachineLearning;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MachineLearningTest
{
	[TestClass]
	public class LinearFunctionTest
	{
		[TestMethod]
		public void TestConstructor()
		{
			var point1 = Vector.Build.Dense(new[] { 0.0, 0.0 });
			var point2 = Vector.Build.Dense(new[] { 1.0, 1.0 });
			var target = new LinearFurction(point1, point2);

			Assert.IsTrue(Math.Abs(0 - target.DistanceFromLine(0.5, 0.5)) < LinearFurction.Epsilon);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void TestConstructorThrowsWithSamePoints()
		{
			var point1 = Vector.Build.Dense(new[] {1.0, 1.0});
			var point2 = Vector.Build.Dense(new[] {1.0, 1.0});
			var target = new LinearFurction(point1, point2);
		}

		[TestMethod]
		public void TestConstructorWithAlmostSamePoints()
		{
			var point1 = Vector.Build.Dense(new[] { 1.0 + 10 * LinearFurction.Epsilon, 1.0 });
			var point2 = Vector.Build.Dense(new[] { 1.0, 1.0 });
			var target = new LinearFurction(point1, point2);
		}

		[TestMethod]
		public void TestDistanceSign()
		{
			var point1 = Vector.Build.Dense(new[] { 0.0, 0.0 });
			var point2 = Vector.Build.Dense(new[] { 1.0, 1.0 });
			var target = new LinearFurction(point1, point2);

			Assert.IsTrue(target.DistanceFromLine(2, 1) < 0);
			Assert.IsTrue(target.DistanceFromLine(2, 0) < 0);
			Assert.IsTrue(target.DistanceFromLine(2, -1) < 0);
			Assert.IsTrue(target.DistanceFromLine(1, -1) < 0);
			Assert.IsTrue(target.DistanceFromLine(0, -1) < 0);
			Assert.IsTrue(target.DistanceFromLine(-1, -2) < 0);
			Assert.IsTrue(target.DistanceFromLine(2, 1.999) < 0);

			Assert.IsTrue(target.DistanceFromLine(1, 2) > 0);
			Assert.IsTrue(target.DistanceFromLine(0, 1) > 0);
			Assert.IsTrue(target.DistanceFromLine(-1, 1) > 0);
			Assert.IsTrue(target.DistanceFromLine(-1, 0) > 0);
			Assert.IsTrue(target.DistanceFromLine(-2, -1) > 0);
			Assert.IsTrue(target.DistanceFromLine(2, 2.001) > 0);
		}

		[TestMethod]
		public void TestDistanceSign2()
		{
			var point1 = Vector.Build.Dense(new[] { -1.0, 0.0 });
			var point2 = Vector.Build.Dense(new[] { 1.0, 1.0 });
			var target = new LinearFurction(point2, point1);

			Assert.IsTrue(target.DistanceFromLine(0, 0) > 0);
			Assert.IsTrue(target.DistanceFromLine(0.7, 0.01) > 0);
		}

		[TestMethod]
		public void TestDistance()
		{
			var point1 = Vector.Build.Dense(new[] {0.0, 0.0});
			var point2 = Vector.Build.Dense(new[] { 1.0, 0.0 });

			var target = new LinearFurction(point1, point2);

			Assert.IsTrue(Math.Abs(1 - target.DistanceFromLine(0, 1)) < LinearFurction.Epsilon);
		}

		[TestMethod]
		public void TestDistanceSignParenthesisInDenominatorBug()
		{
			var point1 = Vector.Build.Dense(new[] { -0.395951890105359, 0.448927269060597 });
			var point2 = Vector.Build.Dense(new[] { 0.92950752560492, 0.907067167529402 });

			var target = new LinearFurction(point1, point2);

			Assert.IsTrue(target.DistanceFromLine(0.648186671383766, -0.751256772201162) > 0);
		}
	}
}
