﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MachineLearning;
using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra.Double;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MachineLearningTest
{
	[TestClass]
	public class LinearRegressionTest
	{
		
		[TestMethod]
		public void MyTestMethod()
		{
			var trials = 1000;
			var outOfSampleErrorSum= 0.0;
			var inSampleErrorSum = 0.0;

			for (int i = 0; i < trials; i++)
			{
				var distribution = new ContinuousUniform(-1.0, 1.0);
				var point1 = Vector.Build.Random(2, distribution);
				var point2 = Vector.Build.Random(2, distribution);
				var targetFunction = new LinearFurction(point1, point2);
				var sample = targetFunction.GenerateSample(distribution, 1000);

				var linearRegression = new LinearRegression();
				linearRegression.Learn(sample);
				var outOfSampleError = linearRegression.OutOfSampleError(vector => targetFunction.DistanceFromLine(vector[0], vector[1]), distribution);
				var inSampleError = linearRegression.InSampleError();

				outOfSampleErrorSum += outOfSampleError;
				inSampleErrorSum += inSampleError;
			}

			var averageOutOfSampleError = outOfSampleErrorSum / trials;
			var averageInSampleError = inSampleErrorSum / trials;

			Assert.IsTrue(true);
		}
	}
}
