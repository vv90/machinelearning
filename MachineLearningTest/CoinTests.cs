﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MachineLearning;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MachineLearningTest
{
	[TestClass]
	public class CoinTests
	{
		[TestMethod]
		public void Test1()
		{
			var rand = new Random();

			double v_1 = 0.0;
			double v_rand = 0.0;
			double v_min = 0.0;

			int trials = 100000;

			for (int k = 0; k < trials; k++)
			{

				var coins = new Coin[1000];
				var headsCount = new int[1000];

				for (int i = 0; i < 1000; i++)
				{
					coins[i] = new Coin();

					int heads = 0;
					for (int j = 0; j < 10; j++)
					{
						if (coins[i].Flip() == CoinValue.Heads)
							heads += 1;
					}

					headsCount[i] = heads;
				}

				v_1 += (double)headsCount[0]/10;
				v_rand += (double) headsCount[rand.Next(1000)]/10;
				v_min += (double)headsCount.Min()/10; 
			}

			v_1 = v_1/trials;
			v_rand = v_rand/trials;
			v_min = v_min/trials;
		}
	}
}
